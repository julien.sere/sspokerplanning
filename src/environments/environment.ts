// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBPl70fkvVNvbuD36rvMMtoFao2WxaZeuA',
    authDomain: 'sspokerplanning.firebaseapp.com',
    databaseURL: 'https://sspokerplanning.firebaseio.com',
    projectId: 'sspokerplanning',
    storageBucket: 'sspokerplanning.appspot.com',
    messagingSenderId: '657509607094',
    appId: '1:657509607094:web:bdf66db3966f6bbf76e817'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
