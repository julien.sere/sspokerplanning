import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateRoomComponent} from './room/create-room.component';
import {RoomComponent} from './room/room.component';
import {ChoosenameComponent} from './pseudo/choosename.component';
import {ListRoomsComponent} from './room/list-rooms.component';


const routes: Routes = [
  { path: '', component: CreateRoomComponent },
  { path: 'create-room', component: CreateRoomComponent },
  { path: 'choose-username', component: ChoosenameComponent },
  { path: 'room/:id', component: RoomComponent },
  { path: 'allrooms', component: ListRoomsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
