import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'sspokerplanning';

  constructor(public router: Router,
              public userService: UserService) {
  }

}
