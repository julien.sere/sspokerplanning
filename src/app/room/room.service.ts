import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {RoomAPI} from '../domain/domain';
import {Observable} from 'rxjs';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(private firestore: AngularFirestore) { }

  public createRoom(room: RoomAPI): Promise<firebase.firestore.DocumentReference<firebase.firestore.DocumentData>> {
    const jsonObject = JSON.parse(JSON.stringify(room));
    return this.firestore
        .collection<RoomAPI>('rooms')
        .add(jsonObject);
  }

  getRoom(id: string): Observable<RoomAPI> {
    return this.firestore.doc<RoomAPI>('rooms/' + id).valueChanges();
  }

  getRoomDocument(id: string): AngularFirestoreDocument<RoomAPI> {
    return this.firestore.doc<RoomAPI>('rooms/' + id);
  }
}
