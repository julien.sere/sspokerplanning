import { Component, OnInit } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {BlobRoomAPI, RoomAPI} from '../domain/domain';

@Component({
  selector: 'app-list-rooms',
  templateUrl: './list-rooms.component.html',
  styleUrls: ['./list-rooms.component.css']
})
export class ListRoomsComponent implements OnInit {

  public rooms: Observable<BlobRoomAPI[]>;

  constructor(private firestore: AngularFirestore) { }

  ngOnInit() {
    this.rooms = this.firestore.collection<BlobRoomAPI>('/blob_rooms').valueChanges({idField: 'id'});
  }

  public parseRoom(value: string): RoomAPI {
    return JSON.parse(value);
  }

  public getRoomLink(room: BlobRoomAPI) {
    return '/room/' + room.id;
  }

  delete(id: string) {
    this.firestore.doc<BlobRoomAPI>('/blob_rooms/' + id).delete();
  }
}
