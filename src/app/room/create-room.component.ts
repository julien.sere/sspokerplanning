import { Component, OnInit } from '@angular/core';
import {RoomAPI} from '../domain/domain';
import {Router} from '@angular/router';
import {BlobRoomService} from '../blob-room.service';

@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html'
})
export class CreateRoomComponent implements OnInit {
  public room: RoomAPI;
  public error: string;
  public isCreating = false;
  public roomName = '';
  public hideGuesses = true;
  public guessSelect = 'standard';

  constructor(private roomService: BlobRoomService, private statefullRoomService: BlobRoomService, private router: Router) { }

  ngOnInit() {
    this.room = new RoomAPI();
  }

  onCreateClick() {
    this.room.guessOptions = this.getGuessOptions();
    this.room.name = this.roomName;
    this.room.roomParameters.hideGuesses = this.hideGuesses;
    this.roomService.createRoom(this.room).then(res => this.creationComplete(res), reason => this.creationFailed(reason));
    this.isCreating = true;
  }

  private creationComplete(res: firebase.firestore.DocumentReference<firebase.firestore.DocumentData>) {
    this.isCreating = false;
    this.router.navigate(['room', res.id]);
  }

  private statefullCreationComplete(id: string|void) {
    this.router.navigate(['room', id]);
  }

  private creationFailed(reason: any) {
    this.isCreating = false;
    this.error = reason;
  }

  private getGuessOptions() {
    switch (this.guessSelect) {
      case 'small': return ['0', '1', '2', '3', '5', '8', '13', '?', '&#x2615;'];
      case 'shirts': return ['XS', 'S', 'M', 'L', 'XL'];
      case 'imsa': return ['0', '1', '2', '3', '5', '8', '13', '20', '50', '100', '&#x2615;'];
      case 'standard': return ['0', '1', '2', '3', '5', '8', '13', '20', '40', '100', '?', '&#x2615;'];
    }
  }
}
