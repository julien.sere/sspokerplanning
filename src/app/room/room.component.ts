import {Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GuessAPI, RoomAPI, UserAPI} from '../domain/domain';
import {AngularFirestoreDocument} from '@angular/fire/firestore';
import {UserService} from '../user.service';
import {BlobRoomService} from '../blob-room.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html'
})
export class RoomComponent implements OnInit {
  public roomDocument: AngularFirestoreDocument<RoomAPI>;
  public id: string;
  public guessOptions = ['1', '3', '5', '8', '11', '13'];
  public userAPI: UserAPI;
  public room: RoomAPI;
  public guessVisible = false;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private roomService: BlobRoomService,
              public userService: UserService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.userAPI = this.userService.getCurrentUser();
    this.roomService.getDocumentRoom(this.id).subscribe(room => this.updatedRoom(room));
  }

  addOrUpdateGuess(value: string) {
    const author = this.userService.getCurrentUser().name;
    this.vote(author, value);
    this.roomService.updateRoom(this.room);
  }

  private vote(author: string, newValue: string) {
    if (!this.room.guesses.find(g => g.author === author)) {
      const newGuess = new GuessAPI();
      newGuess.value = newValue;
      newGuess.author = author;
      this.room.guesses.push(newGuess);
    } else {
      this.room.guesses.find(g => g.author === author).value = newValue;
    }
  }

  public roomLink(): string {
    const getUrl = window.location;
    const baseUrl = getUrl .protocol + '//' + getUrl.host + '/' + getUrl.pathname.split('/')[1];
    return baseUrl + '/room/' + this.id;
  }

  public resetVotes() {
    this.room.guesses = [];
    this.roomService.updateRoom(this.room);
  }

  public changeUsername() {
    this.userService.setUsername(null);
  }

  private updatedRoom(room: RoomAPI) {
    this.room = room;
  }

  public revealGuesses() {
    this.room.guessHidden = false;
    this.roomService.updateRoom(this.room);
  }

  public hideGuesses() {
    this.room.guessHidden = true;
    this.roomService.updateRoom(this.room);
  }

  public copyCurrentUrlToClipboard() {
    const dummy = document.createElement('input');
    const text = window.location.href;
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
  }
}
