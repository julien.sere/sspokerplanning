import { Injectable } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {UserAPI} from './domain/domain';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private POKER_PLANNING_USER_KEY = 'pokerplanninguser';

  constructor(private cookieService: CookieService) { }

  public getCurrentUser(): UserAPI {
    return this.loadLocalStorageUser();
  }

  private loadLocalStorageUser(): UserAPI {
    let user;
    if (window.localStorage.getItem(this.POKER_PLANNING_USER_KEY)) {
      user = JSON.parse(window.localStorage.getItem(this.POKER_PLANNING_USER_KEY));
    } else {
      user = this.newUserAPI();
      this.saveLocalStorageUser(user);
    }
    return user;
  }

  private saveLocalStorageUser(newUser: UserAPI) {
    window.localStorage.setItem(this.POKER_PLANNING_USER_KEY, JSON.stringify(newUser));
  }

  private loadUserCookie() {
    let user = null;
    if (this.userCookieExist()) {
      user = JSON.parse(this.cookieService.get(this.POKER_PLANNING_USER_KEY));
    } else {
      console.log('Création d\'un nouveau user');
      user = this.newUserAPI();
      this.saveCookieUser(user);
    }
    return user;
  }

  private saveCookieUser(user) {
    this.cookieService.set(this.POKER_PLANNING_USER_KEY, JSON.stringify(user));
  }

  public setUsername(username: string) {
    const user = this.getCurrentUser();
    user.name = username;
    this.saveLocalStorageUser(user);
  }

  private userCookieExist() {
    return this.cookieService.check(this.POKER_PLANNING_USER_KEY);
  }

  private newUserAPI() {
    const user = new UserAPI();
    user.id = this.generator();
    user.name = null;
    return user;
  }

  private generator(): string {
    const isString = `${this.S4()}${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}${this.S4()}${this.S4()}`;
    return isString;
  }

  private S4(): string {
    // tslint:disable-next-line:no-bitwise
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  }

  public copyCurrentUrlToClipboard() {
    const dummy = document.createElement('input');
    const text = window.location.href;
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
  }
}
