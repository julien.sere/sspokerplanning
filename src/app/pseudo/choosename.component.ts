import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';

@Component({
  selector: 'app-choosename',
  templateUrl: './choosename.component.html'
})
export class ChoosenameComponent implements OnInit, AfterViewInit {

  constructor(public userService: UserService) { }

  public inputUsername: string;

  ngOnInit() {
    this.inputUsername = this.userService.getCurrentUser().name;
  }

  public changeUsername() {
    this.userService.setUsername(this.inputUsername);
  }

  ngAfterViewInit(): void {
    this.inputUsername = this.userService.getCurrentUser().name;
  }

}
