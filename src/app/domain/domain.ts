export class GuessAPI {
  author: string;
  value: string;
}

export class RoomParametersAPI {
  hideGuesses = true;
}

export class RoomAPI {
  name = 'Sprint 1';
  guesses: GuessAPI[] = [];
  guessOptions = ['1', '3', '5', '8', '11', '13'];
  roomParameters = new RoomParametersAPI();
  guessHidden = true;
}

export class UserAPI {
  id: string;
  name: string;
}

export class BlobRoomAPI {
  id: string;
  value: string;
}
