import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateRoomComponent } from './room/create-room.component';
import { ListRoomsComponent } from './room/list-rooms.component';
import { RoomComponent } from './room/room.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { ChoosenameComponent } from './pseudo/choosename.component';
import {SanitizeHtmlPipe} from './sanitizePipe';

@NgModule({
  declarations: [
    AppComponent,
    CreateRoomComponent,
    ListRoomsComponent,
    RoomComponent,
    ChoosenameComponent,
    SanitizeHtmlPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
