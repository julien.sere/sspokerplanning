import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {BlobRoomAPI, RoomAPI} from './domain/domain';
import * as firebase from 'firebase';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlobRoomService {

  private currentDocument: AngularFirestoreDocument<BlobRoomAPI>;

  constructor(private firestore: AngularFirestore) { }

  public createRoom(room: RoomAPI): Promise<firebase.firestore.DocumentReference<firebase.firestore.DocumentData>> {
    const jsonValue = JSON.stringify(room);

    const blobRoomAPI = new BlobRoomAPI();
    blobRoomAPI.value = jsonValue;

    return this.firestore
      .collection<BlobRoomAPI>('blob_rooms')
      .add(JSON.parse(JSON.stringify(blobRoomAPI)));
  }

  getDocumentRoom(id: string): Observable<RoomAPI> {
    this.currentDocument = this.firestore.doc<BlobRoomAPI>('blob_rooms/' + id);
    return this.currentDocument.valueChanges().pipe(map(blobRoom => this.mapBlobToRoomAPI(blobRoom)));
  }

  public updateRoom(room: RoomAPI) {
    const blobRoom = new BlobRoomAPI();
    blobRoom.value = JSON.stringify(room);
    this.currentDocument.update(JSON.parse(JSON.stringify(blobRoom)));
  }

  getRoom(id: string): Observable<RoomAPI> {
    return this.firestore.doc<BlobRoomAPI>('blob_rooms/' + id).valueChanges().pipe(map(blobRoom => this.mapBlobToRoomAPI(blobRoom)));
  }

  private mapBlobToRoomAPI(blobRoom: BlobRoomAPI): RoomAPI {
    const roomAPI = new RoomAPI();
    const parsedBlob: RoomAPI = JSON.parse(blobRoom.value);
    roomAPI.name = parsedBlob.name;
    roomAPI.guessOptions = parsedBlob.guessOptions;
    roomAPI.guesses = parsedBlob.guesses;
    return roomAPI;
  }
}
